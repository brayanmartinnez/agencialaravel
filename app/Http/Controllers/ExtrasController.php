<?php

namespace App\Http\Controllers;

use App\Models\extras;
use Illuminate\Http\Request;

class ExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extras = extras::all();
        return view ('eventos.extras', compact('extras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $extras = extras::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extras = new extras;
        $extras->nombre = $request->get('nombre');
        $extras->descripcion = $request->get('descripcion');
        $extras->costos = $request->get('costos');
        $extras->save();
        return redirect('/extras');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\extras  $extras
     * @return \Illuminate\Http\Response
     */
    public function show(extras $extras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\extras  $extras
     * @return \Illuminate\Http\Response
     */
    public function edit(extras $extras)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\extras  $extras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, extras $extras)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\extras  $extras
     * @return \Illuminate\Http\Response
     */
    public function destroy(extras $extras)
    {
        //
    }
}

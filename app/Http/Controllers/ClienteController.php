<?php

namespace App\Http\Controllers;

use App\Models\cliente;
use App\Models\estado;
use App\Models\municipio;
use App\Models\parroquia;
use Illuminate\Http\Request;


class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = cliente::all();
        return view('cliente.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = estado::all();
        $municipios = municipio::all();
        $parroquias = parroquia::all();
        return view('cliente.create', compact('estados', 'municipios', 'parroquias'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request    $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientes = new cliente();

        $clientes->nombre = $request->get('nombre');
        $clientes->apellido = $request->get('apellido');
        $clientes->cedula = $request->get('cedula');
        $clientes->direccion = $request->get('direccion');
        $clientes->telefono = $request->get('telefono');
        $clientes->email = $request->get('email');
        $clientes->estados_id = $request->get('estados_id');
        $clientes->municipios_id = $request->get('municipios_id');
        $clientes->parroquias_id = $request->get('parroquias_id');

        $clientes->save();
        
        return redirect('/cliente');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = cliente::findOrFail($id);
        $estado = estado::all();
        $municipio = municipio::all();
        $parroquia = parroquia::all();
        return view('cliente.edit', compact('cliente', $cliente, 'estado', $estado, 'municipio',  $municipio, 'parroquia', $parroquia ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = cliente::find($id);

        $cliente->nombre = $request->get('nombre');
        $cliente->apellido = $request->get('apellido');
        $cliente->cedula = $request->get('cedula');
        $cliente->direccion = $request->get('direccion');
        $cliente->telefono = $request->get('telefono');
        $cliente->email = $request->get('email');
        $cliente->estados_id = $request->get('estados_id');
        $cliente->municipios_id = $request->get('municipios_id');
        $cliente->parroquias_id = $request->get('parroquias_id');

        $cliente->save();
        
        return redirect('/cliente');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = cliente::find($id);
        $cliente->delete();
        return redirect('/cliente');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    use HasFactory;



    public function estado()
    {
        return $this->hasOne(estado::class,);
    }
    
    public function municipio()
    {
        return $this->hasOne(municipio::class);
    }
    
    public function parroquia()
    {
        return $this->hasOne(parroquia::class);
    }
}

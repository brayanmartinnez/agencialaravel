@extends('layouts.app')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="col-sm-12">
            <div class="py-3">
                <div class="px-4">
                    <div class="row justify-center">
                    <h1>listado de clientes</h1>
                    <a href="cliente/create" class="btn btn-primary col-sm-2" >CREAR</a>


<table class="table table-dark table-striped mt-4">
<thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">nombre</th>
                            <th scope="col">apellido</th>
                            <th scope="col">cedula</th>
                            <th scope="col">direccion</th>
                            <th scope="col">telefono</th>
                            <th scope="col">email</th>
                            <th scope="col">cod_estado</th>
                            <th scope="col">cod_municipio</th>
                            <th scope="col">cod_parroquuia</th>
                            <th scope="col">acciones</th>
                            </tr>
                        </thead>
  <tbody>    
  @foreach ($clientes as $key)
                        <tr>
                            <td>{{$key->id}}</td>
                            <td>{{$key->nombre}}</td>
                            <td>{{$key->apellido}}</td>
                            <td>{{$key->cedula}}</td>
                            <td>{{$key->direccion}}</td>
                            <td>{{$key->telefono}}</td>
                            <td>{{$key->email}}</td>
                            <td>{{$key->estados_id}}</td>
                            <td>{{$key->municipios_id}}</td>
                            <td>{{$key->parroquias_id}}</td>
        <td>
         <form action="{{ route('cliente.destroy',$key->id) }}" method="POST">
          <a href="/cliente/{{$key->id}}/edit" class="btn btn-info">Editar</a>         
              @csrf
              @method('DELETE')
          <button type="submit" class="btn btn-danger">Delete</button>
         </form>          
        </td>        
    </tr>
    @endforeach
  </tbody>
</table>
<a href="/" class="btn btn-primary col-sm-2" >REGRESAR</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
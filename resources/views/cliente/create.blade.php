<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Interfaz</title>
  </head>
  <body>
    <div class="container">
      <div class="py-5 text-center">
          <h4 class="mb-3 fs-2">Registro Cliente</h4>
          <form method="post" action="/cliente">
              @csrf
            <div class="row g-3">
              <div class="col-sm-6">
                <label for="firstName" class="form-label">Nombres</label>
                <input type="text" class="form-control" name="nombre" required>
              </div>

              <div class="col-sm-6">
                <label for="lastName" class="form-label">Apellidos</label>
                <input type="text" class="form-control" name="apellido" required>
              </div>

              <div class="col-sm-6">
                <label for="lastName" class="form-label">Numero de Identidad</label>
                <input type="text" class="form-control" name="cedula" required>
              </div>

              <div class="col-sm-6">
                <label for="firstName" class="form-label">Correo Electronico</label>
                <input type="email" class="form-control" name="email" value="" required>
              </div>
              <div class="input-group">
                <span class="input-group-text">Nro Telefono</span>
                <input type="text" name="telefono" class="form-control">
              </div>

              <div class="col-md-4">
                <label for="country" class="form-label">Estado</label>
                <select class="form-select" name="estados_id" required>
                  <option value="">...</option>
                  @foreach ($estados as $key)
                  <option value="{{$key->id}}">{{$key->estado}}</option>
                  @endforeach
                </select>
              </div>
  
              <div class="col-md-4">
                <label for="state" class="form-label">Municipio</label>
                <select class="form-select" name="municipios_id" required>
                  <option value="">...</option>
                  @foreach ($municipios as $key)
                  <option value="{{$key->id}}">{{$key->municipio}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-md-4">
                <label for="country" class="form-label">Parroquia</label>
                <select class="form-select" name="parroquias_id" required>
                  <option value="">...</option>
                  @foreach ($parroquias as $key)
                  <option value="{{$key->id}}">{{$key->parroquia}}</option>
                  @endforeach
                </select>
              </div>

              <div class="input-group">
                <span class="input-group-text">Direccion</span>
                <textarea class="form-control" name="direccion" aria-label="With textarea"></textarea>
              </div>

              <div class="container">
                <div class="btn-group " role="group" aria-label="Basic outlined example">
                <button type="submit" class="btn btn-outline-primary">GUARDAR</button>
                </div>
              </div>

          </div>    
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
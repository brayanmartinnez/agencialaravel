<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Interfaz</title>
  </head>
  <body>
    <div class="container">
      <div class="py-5 text-center">
          <h4 class="mb-3 fs-2">Registro Cliente</h4>
          <form method="post" action="/eventos">
              @csrf
            <div class="row g-3">
              <div class="col-sm-6">
                <label for="firstName" class="form-label">nombre del evento</label>
                <input type="text" class="form-control" name="nombre" required>
              </div>

              <div class="col-sm-6">
                <label for="lastName" class="form-label">fecha</label>
                <input type="date" class="form-control" name="fecha" required>
              </div>


              <div class="col-md-4">
                <label for="country" class="form-label">Direccion</label>
                <select class="form-select" name="direccion_id" required>
                  <option value="">...</option>
                  @foreach ($direccion as $key)
                  <option value="{{$key->id}}">{{$key->direccion}}</option>
                  @endforeach
                </select>
              </div>
  
              <div class="col-md-4">
                <label for="state" class="form-label">catalogo</label>
                <select class="form-select" name="catalogo_id" required>
                  <option value="">...</option>
                  @foreach ($catalogo as $key)
                  <option value="{{$key->id}}">{{$key->nombre}}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-md-4">
                <label for="country" class="form-label">Cobro</label>
                <select class="form-select" name="tipo_cobro_id" required>
                  <option value="">...</option>
                  @foreach ($cobro as $key)
                  <option value="{{$key->id}}">{{$key->tipo_cobro}}</option>
                  @endforeach
                </select>
              </div>

              
              <div class="col-md-4">
                <label for="country" class="form-label">Tipo de pago</label>
                <select class="form-select" name="tipo_pago_id" required>
                  <option value="">...</option>
                  @foreach ($pago as $key)
                  <option value="{{$key->id}}">{{$key->tipo_pago}}</option>
                  @endforeach
                </select>
              </div>


              <div class="container">
                <div class="btn-group " role="group" aria-label="Basic outlined example">
                <button type="submit" class="btn btn-outline-primary">GUARDAR</button>
                </div>
              </div>

          </div>    
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>